# This is an example of a BDT training using an input TTree and Scikitlearn

## The main files are:
* bdtTools.py - a module of functions for the training
* wrapper.py  - implementation of a simple training

## First:
* Check the requirements: python2.7, ROOT 6.12, and python libraries
	* pyroot
	* numpy
	* sklearn
	* cPickle (you can change this by hand if you want)
	* matplotlib
* Read through wrapper.py to check that the *inputs* you want exist. 
  Use `generateSampleTrees.py` to generate inputs if wanted
* You need to specify what weights should be used (the list called *weightNames* in the wrapper.py)
* Modify the pyroot path in bdtTools.py  if needed- I'm lazy and keep mine hard coded here for now
* Check that none of your files will get overwritten

## Setup on LxPlus
There are two options for running this package on lxplus. One option is to setup an LCG view as follows:
```
lsetup "gcc gcc620_x86_64_slc6"
source /cvmfs/sft.cern.ch/lcg/views/dev3/latest/x86_64-slc6-gcc62-opt/setup.sh
```

Whenever logging back into lxplus, execute the above commands again.

The other option is to use asetup + local user installations. Root 6.12 and python2.7 are setup when running:

```
asetup AnalysisBase,21.2.34
```

Unfortunately, sklearn, matplotlib and scipy (!?) will not be included with python2.7. Lets do a local user installation:
```
wget https://bootstrap.pypa.io/get-pip.py && python get-pip.py --user
alias pip="python ~/.local/bin/pip"
pip install scikit-learn==0.19.1 --user
pip install scipy==1.1.0 --user
pip install matplotlib==2.0.2 --user
```

Whenever logging back into lxplus, simply execute "asetup AnalysisBase,21.2.34"


## Expected input
The input is required to be a ROOT file with a TTree containing branches with floats.
In `wrapper.py` example, there are hard-coded names for the file and TTree which correspond to the names in the provided root file

## Preparing input signal vs background sample:
An input root file can be created by using executing the following command:
```
python TrainingInputs.py
```

The file TrainginInputs.py contains a function, taking a list of functions that calculate the training variables, and a list of funcitons that apply the event selections.


## Outputinput
The output plots are written to the directory *plots/*, and the BDT is pickled to *outputBdt.pickle*

## To run:
`python wrapper.py`

## Warning: will overwrite files:
* outputBdt.pickle
* plots/...png

Aaron White

aaronsw@umich.edu

2018
