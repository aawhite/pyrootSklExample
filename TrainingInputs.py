import ROOT
from inputs.samples_sig_bkg import INPUT_sig_ttbar
from array import array
import util
from multiprocessing import Pool

def GenerateTrainingInputs(inputs, outputFilename, variables, variableNames, variableFunctions, selections):
    '''
    A function that creates ready-to-use tree for bdt training. It produces an output root file with a signal and background tree
    The function takes as inputs:
    inputs -- a dictionary of lists of input files. The signal and background files are accessed by strings labeled "sig" and "bkh"
    outputFilename -- a string of the name of the output root file.
    variables -- a list of 1-d arrays that contain the float variables that will be written to the signal and background tree
    variableNames -- a list of strings. Each variable will be written to the output root file with a name taken from variableNames
    variableFunctions -- a list of functions that calculate the output variable from the input trees
    selections -- a list of funcitons that return a boolean value. These functions represent selections applied
    '''

    outputFile = ROOT.TFile(outputFilename, "RECREATE")

    ##Here it is assumed that the dictionary inputs has a list of files that under the key "sig" and "bkg"
    for label in ["sig", "bkg"]:
        ##define the output tree with the training variables
        output_tree = ROOT.TTree(label, label + " tree")

        #create branches for writing the variables
        for variable, variableName in zip(variables, variableNames):
            output_tree.Branch(variableName, variable, variableName + "/F")

        #loop through the list of files
        for filename in inputs[label]:
            input_file = ROOT.TFile(filename, "READ")
            input_file.ls()
            #Here it is assumed that we are reading the nominal tuple. It should be called CxAODTuple_Nominal
            input_tree = input_file.Get("CxAODTuple_Nominal")
            print "Reading from tree " + filename

            count = 0
            #loop through the events
            for event in input_tree:
                if (count % 25000 == 0):
                    print "Processed event ",count
                count+=1

                #loop through the selections that are applied to the event, and continue if the event does not pass the selections, continue
                pass_sel = True
                for selection in selections:
                    if (not selection(event)):
                        pass_sel = False
                        continue

                if not pass_sel:
                    continue

                if not util.passTrigger(event):
                    continue

                #The event has passed selections. Set all of the variables to 0
                for variable in variables:
                    variable[0] = 0.0

                #calculate the values of the output variables, by calling the corresponding functions
                for variable, variableFunction in zip(variables, variableFunctions):
                    variable[0] = variableFunction(event)

                #Fill the tree
                output_tree.Fill()
                ## for jets in event
            ##for event in input_tree
            outputFile.cd()
            output_tree.Write()
        ##for files in inputs["sig"/"bkg"]
    #for label in ["sig", "bkg"]
    output_tree.Write()


#in the future, we could put the selections and variable calculating functions specific files, and call them as need be.
#############Define the event selections#############
def sel_passTrig(event):
    '''return true if the event passed the trigger'''
    return util.passTrigger(event)

def sel_muonPt(event):
    '''muon pT cut'''
    return event.Muons_Pt[0] > 27e3

#############Define the variables writted to thr output tree
def calc_MJets(event):
    '''calculate the invariant mass of all of the jets in the event'''

    all_vectors = ROOT.TLorentzVector()
    for jet_e, jet_phi, jet_eta, jet_pt in zip(event.Jets_Energy, event.Jets_Phi, event.Jets_Eta, event.Jets_Pt):
        vector = ROOT.TLorentzVector()
        vector.SetPtEtaPhiE(jet_pt, jet_eta, jet_phi, jet_e)
        all_vectors+=vector
    return all_vectors.M()

def calc_MBJets(event):
    '''calculate the invariant mass of BJets in the event'''
    all_vectors = ROOT.TLorentzVector()
    for jet_e, jet_phi, jet_eta, jet_pt, b_weight in zip(event.Jets_Energy, event.Jets_Phi, event.Jets_Eta, event.Jets_Pt, event.Jets_BtagWeight):
        if (b_weight > 0.934906):
            vector = ROOT.TLorentzVector()
            vector.SetPtEtaPhiE(jet_pt, jet_eta, jet_phi, jet_e)
            all_vectors+=vector
    return all_vectors.M()

def calc_HT(event):
    '''calculate the HT of all of the jets in the event'''
    return sum(event.Jets_Pt)

def calc_NJets(event):
    '''return the number of jets in the event'''
    return(len(event.Jets_Pt))

def calc_NCentJets(event):
    '''return the number of central jets in the event'''
    return sum([1.0 for jet_eta in event.Jets_Eta if (abs(jet_eta) < 2.5)])

def calc_BJetHT(event):
    '''calculate the HT of all bjets in the event'''
    return sum([jet_pt for jet_pt, b_weight in zip(event.Jets_Pt, event.Jets_BtagWeight) if b_weight > 0.934906])

def calc_ETMiss(event):
    '''calculate the ETMiss of th event'''
    return event.Event_MET

def calc_weight(event):
   '''calculate the weight for the event'''
   return util.getTriggerScaleFactor(event) * event.Weight_TotalWeight

####################An example of how to create the inputs for training###########
if __name__ == "__main__":
    MJets = array('f', [0.])
    HT = array('f', [0.])
    NJets = array('f', [0.])
    NCentJets = array('f', [0.])
    ETMiss = array('f', [0.])
    weight = array('f', [0.])
    trainingVariables = [ MJets ,  HT ,  NJets ,  NCentJets ,  ETMiss ,  weight]
    variableNames =   [ "MJets", "HT", "NJets", "NCentJets", "ETMiss", "weight"]
    variableFunctions = [calc_MJets, calc_HT, calc_NJets, calc_NCentJets, calc_ETMiss, calc_weight]
    selections = [sel_passTrig, sel_muonPt]
    GenerateTrainingInputs(INPUT_sig_ttbar, "TrainingTuple.root", trainingVariables, variableNames, variableFunctions, selections)
