import sys
# remember to point python to the proper place for pyroot
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import ROOT # using ROOT 6.12
import numpy as np
import random, math, os
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.datasets import make_gaussian_quantiles
import cPickle as pickle
import matplotlib.pyplot as plt
import bdtTools
from root_numpy import fill_hist

# Aaron White

# This file has a bunch of functions for training a BDT using a ROOT Ttree

files = [] # file must be kept in global scope (root was written by cavepeople)
def compareDistributions(path,sigName,bkgName,varNames, X, y, weight, bdt=None, varIndex=0, nEventShow=-1):
  ROOT.gROOT.SetStyle('ATLAS')
  names = ["signal", "background"]
  instructions = ["","SAME"]
  colors = [ROOT.kBlue, ROOT.kRed]
  histograms = []
  legend = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)

  ##Draw root histograms to compare the distributions of data vs MC for signal and bkg histograms
  canvas = ROOT.TCanvas(varNames[varIndex], varNames[varIndex])
  canvas.Draw()
  for i, n, c in zip(range(2), names, colors):
    idx = np.where(y == i)
    histograms.append(ROOT.TH1D(n + varNames[varIndex], n + varNames[varIndex], 40, X[:, varIndex].min(), X[:, varIndex].max()))
    fill_hist(histograms[i], X[idx,varIndex][0][0:nEventShow], weight[idx][0:nEventShow])
    histograms[i].Draw(instructions[i])
    histograms[i].SetFillColor(colors[i])
    histograms[i].SetMarkerColor(colors[i])
    histograms[i].SetLineColor(colors[i])
    histograms[i].GetXaxis().SetTitle(varNames[varIndex])
    histograms[i].GetYaxis().SetTitle("arb. units")
    legend.AddEntry(histograms[i], names[i])

  #Draw the canvas comparing the two distributions
  legend.Draw()
  canvas.SetRightMargin(0.15)
  canvas.Modified()
  canvas.Update()
  ##input("Does this look OK?")
  canvas.Print("plots/" + path + ".pdf")

def compare2dDistributions(path, sigName, bkgName, varNames, X, y, weight, bdt=None, varIndex1=0, varIndex2=1, nEventShow=-1):
  ROOT.gROOT.SetStyle('ATLAS')
  names = ["signal", "background"]

  histograms=[]
  ##Draw root histograms to compare the distributions of data vs MC for signal and bkg histograms
  canvas = ROOT.TCanvas(varNames[varIndex1] + varNames[varIndex2], varNames[varIndex1] + varNames[varIndex2])
  canvas.Draw()

  #Loop through the signal and backgrounds
  for i, n in zip(range(2), names):
    idx = np.where(y == i)
    #fill either a signal or background histogram
    histograms.append(ROOT.TH2D(n + "2dHist" + varNames[varIndex1] + varNames[varIndex2], n + varNames[varIndex1] + varNames[varIndex2], 40, X[:, varIndex1].min(), X[:, varIndex1].max(), 40, X[:,varIndex2].min(), X[:,varIndex2].max()))
    fill_hist(histograms[i], X[idx][:, [varIndex1, varIndex2] ][0:nEventShow], weight[idx][0:nEventShow])

  #define a histogram to store the difference between the signal and the background
  diffHist = ROOT.TH2D(n + "diffHist" + varNames[varIndex1] + varNames[varIndex2], n + varNames[varIndex1] + varNames[varIndex2], 40, X[:, varIndex1].min(), X[:, varIndex1].max(), 40, X[:,varIndex2].min(), X[:,varIndex2].max())
  for binx in range(1, diffHist.GetNbinsX()+1):
    for biny in range(1, diffHist.GetNbinsY()+1):
      diffHist.SetBinContent(binx, biny, (histograms[0].GetBinContent(binx,biny) - histograms[1].GetBinContent(binx, biny)) )
  diffHist.GetXaxis().SetTitle(varNames[varIndex1])
  diffHist.GetYaxis().SetTitle(varNames[varIndex2])
  diffHist.GetZaxis().SetTitle("Diff between signal and bkg")
  ROOT.gStyle.SetPalette(53)
  diffHist.Draw("colz")
  #Draw the canvas comparing the two distributions
  canvas.SetRightMargin(0.15)
  canvas.Modified()
  canvas.Update()
  canvas.Print("plots/" + path + "diffhist" + varNames[varIndex1] + varNames[varIndex2] + ".pdf")


def make2dDecisionPlot(path,sigName,bkgName,varNames, X, y, bdt=None, varIndex1=0, varIndex2=1, nEventShow=-1):
  # draw the 2d decision boundary plot
  res=10
  plot_step_x = (X[:, varIndex1].max()-X[:, varIndex1].min() )/res
  plot_step_y = (X[:, varIndex2].max()-X[:, varIndex2].min() )/res

  plt.figure(figsize=(5, 5))
  plt.subplot(111)
  x_min, x_max = X[:, varIndex1].min() , X[:, varIndex1].max()
  y_min, y_max = X[:, varIndex2].min() , X[:, varIndex2].max()
  xx, yy = np.meshgrid(np.arange(x_min, x_max, plot_step_x),
                       np.arange(y_min, y_max, plot_step_y))

  # get average values of all the variables not first two in training
  nVars=len(X[0])
  avgVars = [ reduce(lambda a,b: a+b, [ evnt[iVar] for evnt in X ])/nVars for iVar in range(nVars) ]
  grid = np.zeros((res*res,nVars))
  gridI=0
  xMin=X[:, varIndex1].min()
  xMax=X[:, varIndex1].max()
  yMin=X[:, varIndex2].min()
  yMax=X[:, varIndex2].max()
  xx=[]
  yy=[]
  for xi in range(res):
    xVal=xMin+(xMax-xMin)*xi/res
    xx.append([xVal]*res)
    for yi in range(res):
      yVal=yMin+(yMax-yMin)*yi/res
      if yi==0: yy.append([yVal]*res)
      grid[gridI]=avgVars
      grid[gridI][0]=xVal
      grid[gridI][1]=yVal
      gridI+=1

  class_names = [sigName,bkgName]
  plot_colors = "br"
  # Z = bdt.predict(grid)
  if bdt:
    Z = bdt.decision_function(grid)
    Z = Z.reshape((res,res))
    cs = plt.contourf(xx, yy, Z,alpha=0.5,cmap=plt.cm.hsv)
    # cs = plt.contourf(xx, yy, Z)
    plt.colorbar(cs, shrink=0.8)
    plt.axis("tight")
  # Plot the training points
  for i, n, c in zip(range(2), class_names, plot_colors):
      idx = np.where(y == i)
      plt.scatter(X[idx, varIndex1][0][0:nEventShow], X[idx, varIndex2][0][0:nEventShow],
                  c=c, cmap=plt.cm.Paired,
                  s=20, edgecolor='k', alpha=.5,
                  label="%s" % n)
  plt.xlim(x_min, x_max)
  plt.ylim(y_min, y_max)
  plt.legend(loc='upper right')
  plt.xlabel(varNames[varIndex1])
  plt.ylabel(varNames[varIndex2])
  if bdt:
    plt.title('Decision Boundary')
  else:
    plt.title('Scatter plot of ' + varNames[varIndex1] + ' vs ' + varNames[varIndex2])
  plt.savefig("plots/"+path, bbox_inches='tight')

def makeBarPlot(path,sigName,bkgName,bdt, X, y, weight,ValX, ValY, ValWeight,nBins=20):
  # draw bar plot for BDT output, using X events
  #
  # usage:
  # path = "plots/bar-{0}-{1}-{2}".format(category,sigName,bkgName)
  # makeBarPlot(path,sigName,bkgName,bdt, X, y,weight)
  # path = "plots/scatter-{0}-{1}-{2}".format(category,sigName,bkgName)
  # make2dDecisionPlot(path,sigName,bkgName,varNames,bdt, X, y)
  #
  #remove existing plot
  os.popen("rm plots/{0}*".format(path))
  # decide whether to plot test points
  print "### Plot test points?", type(ValX), type(ValY), type(ValWeight)
  try:
    len(ValX),len(ValY),len(ValWeight)
    plotTest=True
  except:
    plotTest=False
  plot_colors = "br"
  class_names = [bkgName,sigName]
  #
  plt.figure(figsize=(5, 5))
  #
  # Plot the two-class decision scores
  twoclass_output = bdt.decision_function(X)
  if plotTest: testTwoclass_output = bdt.decision_function(ValX)
  plot_range = (twoclass_output.min(), twoclass_output.max())
  plt.subplot(111)
  histMax=0
  for i, n, c in zip(range(2), class_names, plot_colors):
      # make bar plot with training data
      numTrainEvents=len(twoclass_output[y==i])
      binContent,bins,patches = plt.hist(twoclass_output[y == i],
               weights=weight[y==i],
               bins=nBins, 
               #density=True,
               range=plot_range,
               facecolor=c,
               label="Train {0}, n={1}".format(n,numTrainEvents),
               alpha=.5,
               edgecolor='k')
      histMax = max(histMax,max(binContent))
      # make scatter plot with test points
      if plotTest:
        numTestEvents=len(ValWeight[ValY==i])
        binContentUnnormed,binsUnnormed,patchesUnnormed = plt.hist(testTwoclass_output[ValY == i],
               # weights=1,
               # weights=ValWeight[ValY==i],
               bins=nBins,
               # density=False,
               range=plot_range,alpha=0)
        binContent,bins,patches = plt.hist(testTwoclass_output[ValY == i],
               weights=ValWeight[ValY==i],
               bins=nBins,
               # density=True,
               range=plot_range,alpha=0)
        pointPos=[(bins[b]+bins[b+1])/2 for b in range(len(bins)-1)]
        for b in range(len(binContentUnnormed)):# find first non-zero bin to get scale
          if binContentUnnormed[b]>0: break
        normScale=binContent[b]/binContentUnnormed[b]
        plt.plot(pointPos,binContent,"o",color=c,label="Test {0}, n={1}".format(n,numTestEvents))
        # plt.errorbar(pointPos,binContent,linestyle='None',yerr=[np.sqrt(binContentUnnormed),np.sqrt(binContentUnnormed)],color=c,alpha=0.25)
        plt.errorbar(pointPos,binContent,linestyle='None',yerr=np.sqrt(binContentUnnormed)*normScale,color=c,alpha=0.25)
  axes = plt.gca()
  axes.set_ylim(0,histMax*1.2)

  x1, x2, y1, y2 = plt.axis()
  plt.axis((x1, x2, y1, y2 * 1.2))
  plt.legend(loc='upper right')
  plt.ylabel('Samples')
  plt.xlabel('Score')
  plt.title('Decision Scores')
  plt.tight_layout()
  plt.subplots_adjust(wspace=0.35)
  plt.savefig("plots/"+path, bbox_inches='tight')


def makeBarPlot(path,sigName,bkgName,bdt, X, y, weight,ValX, ValY, ValWeight,nBins=20):
  # draw bar plot for BDT output, using X events
  #
  # usage:
  # path = "plots/bar-{0}-{1}-{2}".format(category,sigName,bkgName)
  # makeBarPlot(path,sigName,bkgName,bdt, X, y,weight)
  # path = "plots/scatter-{0}-{1}-{2}".format(category,sigName,bkgName)
  # make2dDecisionPlot(path,sigName,bkgName,varNames,bdt, X, y)
  #
  #remove existing plot
  os.popen("mkdir plots") # plots output
  os.popen("rm plots/{0}*png".format(path))
  # decide whether to plot test points
  print "### Plot test points?", type(ValX), type(ValY), type(ValWeight)
  try:
    len(ValX),len(ValY),len(ValWeight)
    plotTest=True
  except:
    plotTest=False
  plot_colors = "br"
  class_names = [bkgName,sigName]
  #
  plt.figure(figsize=(5, 5))
  #
  # Plot the two-class decision scores
  twoclass_output = bdt.decision_function(X)
  if plotTest: testTwoclass_output = bdt.decision_function(ValX)
  plot_range = (twoclass_output.min(), twoclass_output.max())
  plt.subplot(111)
  histMax=0
  for i, n, c in zip(range(2), class_names, plot_colors):
      # make bar plot with training data
      numTrainEvents=len(twoclass_output[y==i])
      binContent,bins,patches = plt.hist(twoclass_output[y == i],
               weights=weight[y==i],
               bins=nBins,
               # density=True,
               range=plot_range,
               facecolor=c,
               label="Train {0}, n={1}".format(n,numTrainEvents),
               alpha=.5,
               edgecolor='k')
      histMax = max(histMax,max(binContent))
      # make scatter plot with test points
      if plotTest:
        numTestEvents=len(ValWeight[ValY==i])
        binContentUnnormed,binsUnnormed,patchesUnnormed = plt.hist(testTwoclass_output[ValY == i],
               # weights=1,
               # weights=ValWeight[ValY==i],
               bins=nBins,
               # density=False,
               range=plot_range,alpha=0)
        binContent,bins,patches = plt.hist(testTwoclass_output[ValY == i],
               weights=ValWeight[ValY==i],
               bins=nBins,
               # density=True,
               range=plot_range,alpha=0)
        pointPos=[(bins[b]+bins[b+1])/2 for b in range(len(bins)-1)]
        for b in range(len(binContentUnnormed)):# find first non-zero bin to get scale
          if binContentUnnormed[b]>0: break
        normScale=binContent[b]/binContentUnnormed[b]
        plt.plot(pointPos,binContent,"o",color=c,label="Test {0}, n={1}".format(n,numTestEvents))
        # plt.errorbar(pointPos,binContent,linestyle='None',yerr=[np.sqrt(binContentUnnormed),np.sqrt(binContentUnnormed)],color=c,alpha=0.25)
        plt.errorbar(pointPos,binContent,linestyle='None',yerr=np.sqrt(binContentUnnormed)*normScale,color=c,alpha=0.25)
  axes = plt.gca()
  axes.set_ylim(0,histMax*1.2)

  x1, x2, y1, y2 = plt.axis()
  plt.axis((x1, x2, y1, y2 * 1.2))
  plt.legend(loc='upper right')
  plt.ylabel('Samples')
  plt.xlabel('Score')
  plt.title('Decision Scores')
  plt.tight_layout()
  plt.subplots_adjust(wspace=0.35)
  plt.savefig("plots/"+path, bbox_inches='tight')


def ksTest(inputTrainScore,inputTestScore, trainY, testY, oPath=None):
  # return confidence [bkg,sig] that test distribution is sampled from train distribution
  ret={}
  for i in range(2): # for sig and bkg distributions
    trainScore = sorted(inputTrainScore[trainY==i])
    TestScore  = sorted(inputTestScore[testY==i])
    ks=0 # KS statistic
    sumTrain=0 # empirical data distribution
    sumTest=0
    if len(trainScore)==0 or len(TestScore)==0:
      print "\n\n============== KS FAILED ==================", len(trainScore),len(TestScore),oPath,"\n"
      raise BaseException("ksTest tries to divide by zero events")
      return {"conf0":-1,"conf1":-1,"ks0":-1,"ks1":-1}
    addTrain=1/len(trainScore) # number to add to empirical data distribution
    addTest=1/len(TestScore)
    nTrain=len(trainScore)
    nTest=len(TestScore)
    while len(trainScore) or len(TestScore): # while data in sample
      if len(trainScore)==0: # add to sumTest
        sumTest+=addTest
        TestScore.pop(0)
      elif len(TestScore)==0: # add to sumTrain
        sumTrain+=addTrain
        trainScore.pop(0)
      else: # pick which to add to
        if trainScore[0]<TestScore[0]: # add to traiScoren
          sumTrain+=addTrain
          trainScore.pop(0)
        else:
          sumTest+=addTest
          TestScore.pop(0)
      # evaluate KS statistic
      ks = max(ks, abs(sumTrain-sumTest))
    if False: # some debugging print-outs
      print "*"*50
      print "KS result for [bkg,sig][{0}]".format(i)
      print "KS statistic:",ks
      print "c(a=0.5)*sqrt = ", math.sqrt(-0.5*math.log(0.1/2)) * math.sqrt((nTrain+nTest)/(nTrain*nTest))
      print "c(a=0.1)*sqrt = ", math.sqrt(-0.5*math.log(0.1/2)) * math.sqrt((nTrain+nTest)/(nTrain*nTest))
      print "c(a=0.01)*sqrt = ", math.sqrt(-0.5*math.log(0.1/2)) * math.sqrt((nTrain+nTest)/(nTrain*nTest))
    # reject H0 if conf is less than confidence:
    conf = 2 * math.exp( -2*ks**2 * ( (nTrain*nTest)/(nTrain+nTest) ) )
    # print "Can reject H0 if {0} < alpha".format(conf)
    # print "*"*50
    ret["conf{0}".format(i)]=conf
    ret["ks{0}".format(i)]=ks
  return ret



def doTraining(X, y, weight,testX, testY, testWeight,oPath,varNames,depth = 4, loadFromPickle=False, nEst=200):
  ############################################################
  # runs BDT training for inputs, saves output BDT to oPath
  # example usage: bdt=doTraining(X, y, weight, oPath)
  from sklearn import model_selection

  print "#"*100
  if loadFromPickle:
    # skip bdt training, return result from pickle
    print "### Warning: loading from pickle file: ",oPath
    f=open(oPath,"r")
    bdt = pickle.load(f)
    return bdt

  print "Doing BDT training with (sig+bkg) nEvents={0}".format(len(y))
  bdt = AdaBoostClassifier(DecisionTreeClassifier(max_depth=depth),
                           algorithm="SAMME",
                           n_estimators=nEst)
  bdt.fit(X, y, sample_weight=weight)
  # score: Returns the mean accuracy on the given test data and labels.
  print "Simple train score:", bdt.score(testX,testY,testWeight)

  #####################
  # Do KS VALIDATION == <WARNING>
  #####################
  print "#"*20
  # get scores on testX from bdt
  testXBdtScore = bdt.decision_function(testX)
  xBdtScore     = bdt.decision_function(X)
  # chi2Score(xBdtScore,testXBdtScore)
  ksTest(xBdtScore,testXBdtScore, y, testY)
  print "#"*20
  # save the bdt
  print "Saving BDT to", oPath
  f=open(oPath,"w")
  pickle.dump(bdt,f)
  pickle.dump(varNames,f) # save bdt variable names as well
  return bdt


def kSplit(kFolds,X,y,weight):
  # make kFolds splits of X,y,weight
  # return list length kFolds of Xi, yi, weight, XTesti, yTesti, weighti
  ret={"Xi":[], "yi":[], "weighti":[], "XTesti":[], "yTesti":[], "weightTesti":[]}
  X=list(X)
  y=list(y)
  weight=list(weight)
  if len(X)!=len(y) or len(y)!=len(weight):
    raise baseException("Problem with provided size for split")
  nData = len(X)
  n = int(math.ceil(len(X)/kFolds))
  for i in range(kFolds):
    # select testing set
    ret["XTesti"].append(np.array(X[i*n:(i+1)*n]))
    ret["yTesti"].append(np.array(y[i*n:(i+1)*n]))
    ret["weightTesti"].append(np.array(weight[i*n:(i+1)*n]))
    # select training set as exclusion of test
    ret["Xi"].append(np.array(X[0:i*n]+X[(i+1)*n:]))
    ret["yi"].append(np.array(y[0:i*n]+y[(i+1)*n:]))
    ret["weighti"].append(np.array(weight[0:i*n]+weight[(i+1)*n:]))
  # turn everything to np arrays
  ret = {key: np.array(ret[key]) for key in ret.keys()}
  return ret

def getTtree(inputMinitreePath, treeName):
  # get ttree out of a file
  global files # global files to avoid root memory management stepping in
  # open ttree, return
  print "INFO: Opening file", inputMinitreePath
  files.append(ROOT.TFile(inputMinitreePath))
  print "INFO: Getting tree", treeName
  t = files[-1].Get(treeName)
  t.Print()
  return t

def getSigAndBkg(sigData, sigWeights, bkgData, bkgWeights, varNames,maxEventsUse=1e33,normalizeOn=True):
  #############################################################
  # get event data, based on a signal name, background name, category(category)
  # makes lists of all the variables given for each sig/bkg event
  # returns X=variables, y=labels(sig or bkg), weight=weights
  #############################################################

  # set random seeds: must be done HERE (not thread safe)
  random.seed(100) # ensure same set is used for testing
  np.random.seed(100) # ensure same set is used for testing

  # shuffle signal and background, data and weights
  zipSig = zip(sigData,sigWeights)
  random.shuffle(zipSig)
  sigData, sigWeights = zip(*zipSig)
  zipBkg = zip(bkgData,bkgWeights)
  random.shuffle(zipBkg)
  bkgData, bkgWeights = zip(*zipBkg)

  # total counts, for later normalization
  totalSig = sum(sigWeights)
  totalBkg = sum(bkgWeights)

  # datasets for testing (None, by default)
  # merged
  ValX=None
  ValY=None
  ValWeight=None
  # sep for sigData and bkgData
  valSig=None
  valBkg=None
  valSigWeights=None
  valBkgWeights=None

  # trim numbers of events, leftovers used for testing
  minEventsForTest=int(0.2*min(len(sigData),len(bkgData)))
  print "sigData",len(sigData)
  print "sigWeights",len(sigWeights)
  print "bkgData",len(bkgData)
  print "bkgWeights",len(bkgWeights)
  if len(sigData)>maxEventsUse+minEventsForTest:
    sigData,valSig=sigData[0:maxEventsUse], sigData[maxEventsUse:]
  else:
    sigData,valSig=sigData[0:-minEventsForTest], sigData[-minEventsForTest:]
  if len(sigWeights)>maxEventsUse+minEventsForTest:
    sigWeights,valSigWeights=sigWeights[0:maxEventsUse], sigWeights[maxEventsUse:]
  else:
    sigWeights,valSigWeights=sigWeights[0:-minEventsForTest], sigWeights[-minEventsForTest:]
  if len(bkgData)>maxEventsUse+minEventsForTest:
    bkgData,valBkg=bkgData[0:maxEventsUse], bkgData[maxEventsUse:]
  else:
    bkgData,valBkg=bkgData[0:-minEventsForTest], bkgData[-minEventsForTest:]
  if len(bkgWeights)>maxEventsUse+minEventsForTest:
    bkgWeights,valBkgWeights=bkgWeights[0:maxEventsUse], bkgWeights[maxEventsUse:]
  else:
    bkgWeights,valBkgWeights=bkgWeights[0:-minEventsForTest], bkgWeights[-minEventsForTest:]


  # normalize lists of weights to both have total 1
  # I think this is a good idea
  if normalizeOn:
    sigWeights/=np.sum(sigWeights)
    bkgWeights/=np.sum(bkgWeights)
    valSigWeights/=np.sum(valSigWeights)
    valBkgWeights/=np.sum(valBkgWeights)

  # combine sigData and bkgData into one array, also weights
  X             = np.concatenate((sigData,bkgData))
  weight = np.concatenate((sigWeights,bkgWeights))
  ValX = np.concatenate((valSig,valBkg))
  ValWeight = np.concatenate((valSigWeights,valBkgWeights))

  # make array of class label
  y             = np.array([1]*len(sigData)+[0]*len(bkgData))
  ValY          = np.array([1]*len(valSig)+[0]*len(valBkg))

  # shuffle train/test and val samples
  zipTest = zip(X,weight,y)
  random.shuffle(zipTest)
  X,weight,y = zip(*zipTest)
  zipVal = zip(ValX,ValWeight,ValY)
  random.shuffle(zipVal)
  ValX,ValWeight,ValY = zip(*zipVal)
  # turn back into arrays
  X=np.array(X)
  weight=np.array(weight)
  y =np.array(y )
  ValX=np.array(ValX)
  ValWeight=np.array(ValWeight)
  ValY =np.array(ValY )

  return X, y, weight, ValX, ValY, ValWeight, totalSig,totalBkg



def readTtreeToArray(t,weightNames, skip=False):
  # read ttree variables into arrays
  # return weights, np array [variables, events]
  # enable skip for testing, use random data

  # set up objects to store ttree data
  # list of branch names in tree: 
  varNames = [br.GetName() for br in t.GetListOfBranches()]
  varNames = filter(lambda br: br not in weightNames, varNames)
  varNames = sorted(varNames)
  varData = [] # list organized by event, or lists organized by varNames
  weights = [] # list organized by event

  # read training variables, weights from tree
  for entryi, entry in enumerate(t):
    # load trianing variables
    if skip:
      if random.randint(0,1): continue # skip random events
    varData.append([]) # add new row to varData
    for name in varNames:
      if skip: varData[-1].append(random.randint(0,500)) # append garbage data
      else: varData[-1].append(getattr(entry,name))
    # load weights
    weight = reduce(lambda a,b:a*b,[getattr(entry,name) for name in weightNames])
    weights.append(weight)
    if entryi == 2000: break

  # convert to numpy arrays, return
  weightArray = np.array(weights)
  dataArray   = np.array(varData)
  return weightArray, dataArray, varNames

def prepData(weightNames,inputMinitreePath_sig,treeName_sig,inputMinitreePath_bkg,treeName_bkg, skip=False):
  # prep data arrays with input of root path, tree name
  # returns weights, data for both sig and bkgData
  # sig
  sigTree = getTtree(inputMinitreePath_sig,treeName_sig)
  sigWeights, sigData, sigVarNames = readTtreeToArray(sigTree, weightNames, skip=skip)
  # bkgData
  bkgTree = getTtree(inputMinitreePath_bkg,treeName_bkg)
  bkgWeights, bkgData, bkgVarNames = readTtreeToArray(bkgTree, weightNames, skip=skip)
  # check sig and bkg var names same
  if sigVarNames != bkgVarNames: raise BaseException("Miss-matched branch names in input")
  return sigVarNames, sigWeights, sigData, bkgWeights, bkgData

