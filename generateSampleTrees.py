from __future__ import division
import sys
# remember to point python to the proper place for pyroot
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path

from ROOT import TFile, TTree
from array import array
import random, math, os

# script to generate sample root file for testing bdt training

os.popen("rm ntuple.root")
f = TFile( 'ntuple.root', 'recreate' )

# signal is 2d gaussian centered around 0,0
sigTree = TTree( 'sig', 'sig tree' )
# bkg is circular ring distribution, radius ~2
bkgTree = TTree( 'bkg', 'bkg tree' )

# set up tree
var1 = array( 'f', [ 0.0 ] )
var2 = array( 'f', [ 0.0 ] )
weight = array( 'f', [ 0.0 ] )
sigTree.Branch( 'var1', var1, 'var1/F' )
sigTree.Branch( 'var2', var2, 'var2/F' )
sigTree.Branch( 'weight', weight, 'weight/F' )
bkgTree.Branch( 'var1', var1, 'var1/F' )
bkgTree.Branch( 'var2', var2, 'var2/F' )
bkgTree.Branch( 'weight', weight, 'weight/F' )

n = 10000
weight[0] = 1
xs,ys,xb,yb=[],[],[],[]
for i in range(n):
   # set up signal
   var1[0] = random.gauss(0,1)
   var2[0] = random.gauss(0,1)
   xs.append(random.gauss(0,1))
   ys.append(random.gauss(0,1))
   sigTree.Fill()

   # set up background
   # pick angle, radius
   angle=random.randint(0,360*100)/100
   radius=random.gauss(4,1)
   var1[0] = radius * math.sin(angle/math.pi)
   var2[0] = radius * math.cos(angle/math.pi)
   xb.append( radius * math.sin(angle/math.pi))
   yb.append( radius * math.cos(angle/math.pi))
   bkgTree.Fill()

# import matplotlib.pyplot as plt
# plt.plot(xs,ys,"r,")
# plt.plot(xb,yb,"b,")
# plt.show()

sigTree.Write()
bkgTree.Write()
f.Write()
f.Close()
