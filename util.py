import ROOT

def getTriggerScaleFactor(event):
    trigSF = 1.0
    if (event.Event_RunNumber <= 24484):
        if (event.Event_passHLT_mu20_iloose_L1Mu15):
            trigSF = event.Weight_trigSF_HLT_mu20_iloose_L1MU15
    else:
        if (event.Event_passHLT_mu26_ivarmedium):
            trigSF = event.Weight_trigSF_HLT_mu26_ivarmedium
    if event.Event_passHLT_mu50:
        trigSF = event.Weight_trigSF_HLT_mu50

    if trigSF > 0.0:
        return trigSF
    else:
        return 1.0

def passTrigger(event):
    return  event.Event_passHLT_mu20_iloose_L1Mu15 or event.Event_passHLT_mu50
