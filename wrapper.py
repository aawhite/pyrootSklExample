from __future__ import division
import sys
# remember to point python to the proper place for pyroot
#sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path

import ROOT # using ROOT 6.12
import numpy as np
import random, math, os
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.datasets import make_gaussian_quantiles
import cPickle as pickle
import matplotlib.pyplot as plt
import bdtTools
from itertools import combinations
# Aaron White

print '''
***********************************************************************************
*    This python script runs a BDT training based on any ttree of floats          *
*    This is the main script to run with "python2.7 wrapper.py"                   *
*                                                                                 *
*    It will:                                                                     *
*      1) Read all the variables out of the ttrees                                *
*      2) Shuffle, trim these im preparation for BDT training                     *
*         A train, test, and validation set are made                              *
*      3) Perform BDT training                                                    *
*      4) Make a quick histogram with test/train info                             *
*                                                                                 *
*    This is meant as an example                                                  *
*                                                                                 *
*    ### Warning - it will delete some plots in the directory ./plots/            *
*    !!! Please change the path to pyroot in bdtTools                             *
*    $$$ Note that the flag "skip" will use pseudo-data for the training...       *
*    *** Please change the "input specific names" to whatever you plan to run on  *
*                                                                                 *
*    Enter to continue                                                            *
***********************************************************************************
''' 
raw_input()

if __name__ == "__main__":
  ######################################################################################

  # $$$$$$$$$$$$$$
  # Step 0
  # $$$$$$$$$$$$$$

  # input specific names (should be changed before running)
  inputMinitreePath="TrainingTuple.root" # name of input root file
  sigTreeName="sig" # name of tree in input
  bkgTreeName="bkg" # name of tree in input
  # names of wieghts
  weightNames = ["weight"]

  # generic inputs (don't need to be changed to run)
  oPath="outputBdt.pickle" # bdt output path
  skip=False # use pseudo data
  #maxEventsUse=-1 # max events for training
  sigName = "signal"
  bkgName = "background"

  maxEvents=1000 # max events for training

  # $$$$$$$$$$$$$$
  # Step 1
  # $$$$$$$$$$$$$$
  # prepare inputs
  varNames,sigWeights,sigData,bkgWeights,bkgData=bdtTools.prepData(weightNames,inputMinitreePath,sigTreeName,inputMinitreePath,bkgTreeName, skip=skip)

  # $$$$$$$$$$$$$$
  # Step 2
  # $$$$$$$$$$$$$$
  # split by signal, validation
  X, y, weight, ValX, ValY, ValWeight,totalSig,totalBkg = bdtTools.getSigAndBkg(sigData, sigWeights, bkgData, bkgWeights, varNames)
  # do ksplit?
  if True: # use ksplit of training data for testing
    print "#### TESTING SET BEING MADE ####"
    splits = bdtTools.kSplit(10, X,y,weight)
    # relabel inputs
    X = splits["Xi"][0]
    y = splits["yi"][0]
    weight = splits["weighti"][0]
    testX = splits["XTesti"][0]
    testY = splits["yTesti"][0]
    testWeight = splits["weightTesti"][0]

  # $$$$$$$$$$$$$$
  # Step 3
  # $$$$$$$$$$$$$$
  # do training
  bdt=bdtTools.doTraining(X, y, weight,testX, testY, testWeight, oPath,varNames,depth=3,nEst=20,loadFromPickle=False)

  # $$$$$$$$$$$$$$
  # Step 4
  # $$$$$$$$$$$$$$
  # sample plot
  path = "output-hist.png"
  bdtTools.makeBarPlot(path,sigName,bkgName,bdt, X, y,weight,testX, testY, testWeight,nBins=20)
  path = "output-scatter.png"
  combs = combinations(range(0, len(varNames)), 2)
  for (i1, i2) in combs:
    path = "outputHistograms" + varNames[i1] + varNames[i2]
    bdtTools.make2dDecisionPlot(path,sigName,bkgName,varNames,X,y,bdt, varIndex1=i1, varIndex2=i2)
  for i in range(0, len(varNames)):
   path = "outputHistograms" + varNames[i]
   bdtTools.compareDistributions(path,sigName,bkgName,varNames, X, y, weight, varIndex=i, nEventShow=-1)

  # sometimes pyroot crashes on exit - it doesn't matter, just print this
  print "="*100
  print "done"
  print "="*100

  ######################################################################################
